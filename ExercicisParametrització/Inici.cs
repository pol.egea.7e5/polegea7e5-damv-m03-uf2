﻿namespace ExercicisParametrització
{
    internal class Parametritzacio
    {
        public static void Main()
        {
            var program = new Parametritzacio();
            program.Inici();
        }

        public void Inici()
        {
            bool fi;
            do
            {
                Menu.MostraMenu();
                fi = Menu.ExecutaOpcio();
            } while (!fi);
        }
    }
}