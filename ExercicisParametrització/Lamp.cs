using System;
namespace ExercicisParametrització
{
    public class Lamp
    {
        public static void Inici()
        {
            string ordre = null;
            bool lampada = false;
            IntrodueixDades(ref ordre,ref lampada);
        }
        // ReSharper disable once RedundantAssignment
        public static void IntrodueixDades(ref string ordre, ref bool lampada)
        {
            do
            {
                Console.WriteLine("Introdueixi l'ordre");
                ordre = Console.ReadLine(); 
                Console.WriteLine(TractamentOpcio(ordre, ref lampada));
            } while (ordre != "END");
        }
        public static bool TractamentOpcio(string ordre, ref bool lampada)
        {
            switch (ordre)
            {
                case "TURN OFF":
                    lampada = false;
                    break;
                case "TURN ON":
                    lampada = true;
                    break;
                case "TOGGLE":
                    lampada = !lampada;
                    break;
            }
            return lampada;
        }
    }
}