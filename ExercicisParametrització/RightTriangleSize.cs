using System;

namespace ExercicisParametrització
{
    public class RightTriangleSize
    {
        public static void Inici()
        {
            int valor =  NumTriangles();
            double[] baseTriangle = new double[valor];
            double[] alturaTriangle=new double[valor];
            IntroduirDades(ref valor,baseTriangle,alturaTriangle);
            ExecutaDades(ref valor,baseTriangle,alturaTriangle);
        }

        static int NumTriangles()
        {
            int valor;
            do
            {
                Console.WriteLine("Introdueixi el número de triangles");
                valor = Convert.ToInt32(Console.ReadLine());
            } while (valor < 1);
            return valor;
        }

        public static void IntroduirDades(ref int valor,double[] baseTriangle,double[] alturaTriangle)
        {
            for (int i = 0; i < valor; i++)
            {
                Console.WriteLine("Introdueixi base del triangle {0}",i+1);
                baseTriangle[i] = Convert.ToDouble(Console.ReadLine());
                Console.WriteLine("Introdueixi altura del triangle {0}",i+1);
                alturaTriangle[i] = Convert.ToDouble(Console.ReadLine());
            }

        }
        public static void ExecutaDades(ref int valor,double[] baseTriangle,double[] alturaTriangle)
        {
            for (int i = 0; i < valor; i++)
            {
                Console.WriteLine("Un triangle de {0} X {1} té {2}² d'area i {3} de perímetre",baseTriangle[i],alturaTriangle[i],CalculArea(baseTriangle[i],alturaTriangle[i]),CalculPerimetre(baseTriangle[i]));
            }
        }
        static double CalculArea(double baseT,double alturaT)
        {
            
            return ((baseT*alturaT)/2);
        }

        static double CalculPerimetre(double baseT)
        {
            return baseT * 3;
        }
    }
}