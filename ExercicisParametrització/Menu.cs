using System;

namespace ExercicisParametrització
{
    public class Menu
    {
        public static void MostraMenu()
        {
            Console.WriteLine("    ___                                            _                _      _                                _           ");
            Console.WriteLine("   | _ \\   __ _     _ _    __ _   _ __     ___    | |_      _ _    (_)    | |_      ___    __ _    __      (_)     ___  ");
            Console.WriteLine("   |  _/  / _` |   | '_|  / _` | | '  \\   / -_)   |  _|    | '_|   | |    |  _|    |_ /   / _` |  / _|     | |    / _ \\ ");
            Console.WriteLine("  _|_|_   \\__,_|  _|_|_   \\__,_| |_|_|_|  \\___|   _\\__|   _|_|_   _|_|_   _\\__|   _/__|   \\__,_|  \\__|_   _|_|_   \\___/ ");
            Console.WriteLine("_| \"\"\" |_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|_|\"\"\"\"\"|");
            Console.WriteLine("\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'\"`-0-0-'");
            Console.WriteLine("#####################################################################################################################################################################");
            Console.WriteLine("RT-Right Triangle Size");
            Console.WriteLine("LA-Lamp");
            Console.WriteLine("CO-Campsite Organizer");
            Console.WriteLine("BR-Basic Robot");
            Console.WriteLine("TR-Three in a Row");
            Console.WriteLine("SC-Squash Counter");
            Console.WriteLine("FI-Finalitza Programa");
            Console.WriteLine();
        }
        public static bool ExecutaOpcio()
        {
            bool fi = false;
            string opcio = Console.ReadLine()?.ToUpper();
            switch (opcio)
            {
                case "RT":
                    RightTriangleSize.Inici();
                    break;
                case "LA":
                    Lamp.Inici();
                    break;
                case "CO":
                    CampsiteOrganizer.Inici();
                    break;
                case "BR":
                    BasicRobot.Inici();
                    break;
                case "TR":
                    ThreeinaRow.Inici();
                    break;
                case "SC":
                    SquashCounter.Inici();
                    break;
                case "FI":
                    fi = true;
                    break;
            }
            return fi;
        }
    }
}