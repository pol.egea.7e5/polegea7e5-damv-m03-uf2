using System;
using System.Linq;

namespace ExercicisParametrització
{
    public class CampsiteOrganizer
    {
        public static void Inici()
        {
            string[] nomsParcela = new string[1];
            int[] persones = new int[1];
            int reservesTotals = 0;
            while (!IntrodueixDades(ref nomsParcela, ref persones, ref reservesTotals))
            {
                Console.WriteLine("Parcel·les: {0}",nomsParcela.Count()-1);
                Console.WriteLine("Persones: {0}",persones.Sum());
            }
        }

        public static bool IntrodueixDades(ref string[] nomsParcela, ref int[] persones, ref int reservesTotals)
        {
            int persona;
            string nom;
            bool fi=false;
            string ordre1=Introdueix1AOrdre();
            switch (ordre1)
            {
                case "ENTRA":
                    persona = IntrodueixPersones();
                    nom = IntrodueixNom();
                    SolicitudEntra(ref nomsParcela,ref persones,ref reservesTotals,nom,persona);
                    break;
                case "MARXA":
                    nom = IntrodueixNom();
                    SolicitudBaixa(ref nomsParcela,ref persones,ref reservesTotals,nom);
                    break;
                case "END":
                    fi = true;
                    break;
            }

            return fi;
        }

        public static string Introdueix1AOrdre()
        {
            string nom;
            do
            {
                Console.WriteLine("Introdueix Ordre de reserva");
                nom = Console.ReadLine();

            } while (nom!="END"&&nom!="ENTRA"&&nom!="MARXA");

            return nom;
        }
        public static int IntrodueixPersones()
        {
            int nom;
            do
            {
                Console.WriteLine("Introdueix Numero de persones");
                nom = Convert.ToInt32(Console.ReadLine());

            } while (nom<1);

            return nom;
        }
        public static string IntrodueixNom()
        {
            Console.WriteLine("Introdueix Nom de reserva");
            string nom = Console.ReadLine();
            return nom;
        }

        public static void SolicitudEntra(ref string[]nomsParcela,ref int[]persones,ref int reservesTotals,string nom,int persona)
        {
            nomsParcela[reservesTotals] = nom;
            persones[reservesTotals] = persona;
            ++reservesTotals;
            Array.Resize(ref nomsParcela,reservesTotals+1);
            Array.Resize(ref persones,reservesTotals+1);
        }
        public static void SolicitudBaixa(ref string[]nomsParcela,ref int[]persones,ref int reservesTotals,string nom)
        {
            int posinom=Array.IndexOf(nomsParcela, nom);
            for (int i = posinom; i < reservesTotals; i++)
            {
                nomsParcela[i] = nomsParcela[i+1];
                persones[i] = persones[i+1];
            }
            Array.Resize(ref nomsParcela,reservesTotals);
            Array.Resize(ref persones,reservesTotals);
            reservesTotals --;
        }
    }
}